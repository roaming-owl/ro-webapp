import './css/base.css';
import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import environment from './app/environment';
import app from './state/app';
import Router from './components/router'
import mapScreen from './screens/map';
import searchScreen from './screens/search';
import scanScreen from './screens/scan';
import settingsScreen from './screens/settings';
import I18n from './core/i18n'; //TODO: find a better loacation, but translations are not workin if it isnt here
import dataManager from './core/dataManager';
import appTheme from './app/theme';
const INIT_STEP = 'init';
const INSTALL_STEP = 'installing';
const POSTINSTALL_STEP = 'postinstall';
const COMPLETE_STEP = 'complete';

export default class extends Component {
    constructor(params) {
        super(params);

        console.log(this.theme);

        this.state = {
            step: INIT_STEP
        }
    }

    registerServiceWorker() {
        return Promise.resolve();
    }

    async checkForUpdate() {
        return dataManager.sync();
    }

    async componentDidMount() {
        await environment.recoginze()
        app.registerScreen(mapScreen);
        app.registerScreen(searchScreen);
        app.registerScreen(scanScreen);
        app.registerScreen(settingsScreen);

        await this.checkForUpdate();
        await this.registerServiceWorker();
        this.setState({step: COMPLETE_STEP});
    }

    render() {
        if (this.state.step !== COMPLETE_STEP) {
            return (<div>[img loading...]</div> );
        }
        return (
            <MuiThemeProvider muiTheme={appTheme}>
                <Router screens={app.getScreens()}/>
            </MuiThemeProvider>
        );
    }
}
