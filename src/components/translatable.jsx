import React from 'react';
import events from './../core/events';
import I18n from './../core/i18n';
import appState from './../state/app';

export default function (Component) {
    return class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                locale: null
            }
        }

        onLanguageChange = (locale) => {
            this.setState({locale: locale});
        };

        componentDidMount() {
            appState.on(events.APPLICATION.LANGUAGE_CHANGED, this.onLanguageChange);
        }

        componentWillUnmount() {
            appState.off(events.APPLICATION.LANGUAGE_CHANGED, this.onLanguageChange);
        }

        render() {
            let props = {
                ...this.props,
                t: (key, replacements) => {
                    return I18n.t(key, replacements)
                }
            }
            return (<Component {...props}></Component>)
        }
    }
}