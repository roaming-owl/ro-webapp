import React from 'react';
import MaterialAppBar from 'material-ui/AppBar';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import IconButton from 'material-ui/IconButton';
import sidebar from './../state/sidebar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import defaultTheme from './../app/theme';

export default class AppBar extends React.Component {

    getTitle() {
        //TODO: appState.state.settings[appSettings.BASE_URL]
        return 'Registry';
    }

    getIconElementRight() {
        return null;
    }

    getIconElementLeft() {
        return <IconButton onClick={this.onLeftButtonClicked}><NavigationMenu/></IconButton>;
    }

    render() {
        let styles = {
            appBar: {
                flexShink: 0,
            },
            appBarFixed: {
                position: 'fixed'
            }
        }
        return (
            <MuiThemeProvider muiTheme={(this.state && this.state.theme) || defaultTheme}>
                <MaterialAppBar
                    style={styles.appBarFixed}
                    // TODO: if className not present, it force display: -webkit-box into root style component of appBar. has something to do with autoprefixer
                    className="app-bar-flex-fix"
                    title={this.getTitle()}
                    iconElementLeft={this.getIconElementLeft()}
                    iconElementRight={this.getIconElementRight()}
                />
            </MuiThemeProvider>
        );
    }

    onLeftButtonClicked = () => {
        sidebar.open();
    }
}