import React from 'react';
import Drawer from 'material-ui/Drawer';
import {withRouter} from 'react-router-dom';
import Events from './../core/events';
import sidebar from './../state/sidebar';
import AppRouter from './../core/router';
import appState from './../state/app';
import {MenuItem} from "material-ui";

class Sidebar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: sidebar.state.open,
            screens: appState.getScreens()
        }
    }

    componentDidMount() {
        sidebar.on(Events.SIDEBAR.OPEN_CHANGING, this.toggleOpen);
    }

    componentWillUnmount() {
        sidebar.off(Events.SIDEBAR.OPEN_CHANGING, this.toggleOpen);
    }

    toggleOpen = (open) => {
        console.log('toggle', open);
        let drawer = document.querySelector('.app-sidebar div:nth-child(2)');
        drawer.addEventListener('transitionend', () => {
            sidebar.emit(Events.SIDEBAR.OPEN_CHANGED, open);
        }, {once: true});
        this.setState({open: open});
    }

    onMenuItemClicked = (screen) => {
        sidebar.close()
        .then(() => {
            AppRouter.openScreen(screen);
        })
    }

    getMenuItemComponent(screen) {
        return (<MenuItem key={screen.code} onClick={() => this.onMenuItemClicked(screen)} style={{
            backgroundColor: this.props.location.pathname === AppRouter.getRouteTo(screen) ? '#eee' : '#fff'
        }}>{screen.menuItem.label}</MenuItem>)
    }

    getScreensWithMenuItems() {
        return ['scan', 'map', 'search', 'settings'];
        // return Object.keys(this.state.screens)
        // .filter((screenKey) => {
        //     return this.state.screens[screenKey].menuItem !== undefined;
        // });
    }

    render() {
        return (
            <Drawer className="app-sidebar" docked={false} open={this.state.open}
                    onRequestChange={(open) => this.setState({open: open})}>
                {this.getScreensWithMenuItems().map((screenKey) => {
                    return this.getMenuItemComponent(this.state.screens[screenKey]);
                })}
            </Drawer>
        )
    }
}

/*

 */

let AppSidebar = withRouter((props) => {
    return <Sidebar {...props}/>
})

export default {AppSidebar, Sidebar};