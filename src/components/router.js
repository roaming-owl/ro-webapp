import React from 'react';
import {
    Router as Router,
    Redirect,
    Route
} from 'react-router-dom';
import appRouter from './../core/router';
import mapScreen from './../screens/map';
import history from './history';


export default class extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            routes: Object.keys(this.props.screens).map((screenCode) => {
                return {
                    path: '/' + this.props.screens[screenCode].code,
                    screen: this.props.screens[screenCode].content,
                    appBar: this.props.screens[screenCode].appBar,
                    sidebar: this.props.screens[screenCode].sidebar
                }
            })
        }
    }

    render() {
        return (
            <Router history={history}>
                <div>
                    <Route
                        key='index'
                        path='/'
                        exact={true}
                        component={() => <Redirect to={appRouter.getRouteTo(mapScreen)}/>}
                    />
                    {this.state.routes.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={route.appBar}
                        />
                    ))}
                    {this.state.routes.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={route.sidebar}
                        />
                    ))}
                    {this.state.routes.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={route.screen}
                        />
                    ))}
                </div>
            </Router>
        )
    }
}