import asyncComponent from './../components/asyncComponent';
import AppBar from './search/appBar';
//import Sidebar from './../components/sidebar';
import Sidebar from './search/sidebar';

const AsyncSearchScreen = asyncComponent(() => import('./components/search'));

const CODE = 'search';

const screen = {
    code: CODE,
    icon: null,
    content: AsyncSearchScreen,
    appBar: AppBar,
    sidebar: Sidebar,
    appTitle: CODE,
    menuItem: {
        label: CODE
    }
}

export default screen;