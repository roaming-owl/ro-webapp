import React from 'react';
import AppBar from './../../components/appBar';
import {IconMenu, MenuItem, TextField} from "material-ui";
import IconButton from 'material-ui/IconButton';
import QrCode from './../../svgIcons/qrCode';
import screenState from './../../state/scanScreen';

export default class SearchAppBar extends AppBar {
    getTitle() {
        return (
            <div>Scan QR code</div>
        )
    }

    openScanHistory = () => {
        screenState.showHistoryDialog();
    }

    getIconElementRight() {
        return (
            <IconMenu
                iconButtonElement={<IconButton><QrCode/> </IconButton>}
                anchorOrigin={{vertical: 'top', horizontal: 'left'}}
            >
                <MenuItem primaryText="Scan history..." onClick={this.openScanHistory} />
            </IconMenu>
        )
    }
}