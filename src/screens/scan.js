import asyncComponent from './../components/asyncComponent';
import AppBar from './scan/appBar';
import Sidebar from './../components/sidebar';

const AsyncScanScreen = asyncComponent(() => import('./components/scan'));

const CODE = 'scan';

const screen = {
    code: CODE,
    icon: null,
    content: AsyncScanScreen,
    appBar: AppBar,
    sidebar: Sidebar.AppSidebar,
    appTitle: CODE,
    menuItem: {
        label: CODE
    }
}

export default screen;