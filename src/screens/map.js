import asyncComponent from './../components/asyncComponent';
import AppBar from './map/appBar';
import Sidebar from './map/sidebar';

const AsyncMapScreen = asyncComponent(() => import('./components/map'));

const CODE = 'map';

const screen = {
    code: CODE,
    icon: null,
    content: AsyncMapScreen,
    appBar: AppBar,
    sidebar: Sidebar,
    appTitle: CODE,
    menuItem: {
        label: CODE
    }
};

export default screen;