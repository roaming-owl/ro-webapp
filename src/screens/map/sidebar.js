import React from 'react';
import Sidebar from './../../components/sidebar';
import screenInstance from './../map';
import Divider from 'material-ui/Divider';
import {withRouter} from 'react-router-dom';

class MapSidebar extends Sidebar.Sidebar {
    //TODO: maybe we can remove this and all per-screen sidbars?
}

export default withRouter((props) => {
    return <MapSidebar {...props}/>
})