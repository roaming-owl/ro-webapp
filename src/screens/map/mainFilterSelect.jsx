import React from 'react';
import {MenuItem, RadioButton, RadioButtonGroup, SelectField} from "material-ui";
import MapFilter from './../../core/mapFilter';
import screenState from './../../state/mapScreen';
import events from './../../core/events';
import I18n from './../../core/i18n';

export default class MainFilterSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ...screenState.state
        };
    }

    componentDidMount() {
        screenState.on(events.STATE.CHANGED, this.onScreenStateChange);
    }

    componentWillUnmount() {
        screenState.off(events.STATE.CHANGED, this.onScreenStateChange);
    }

    onScreenStateChange = (newState) => {
        this.setState({...newState});
    }

    onSubfilterChange = (evt, newVale) => {
        screenState.setFilter(this.state.mainFilter, newVale);
    }

    onMainFilterChange = (evt, idx, newValue) => {
        if (MapFilter.filters[newValue].$subFilters && Object.keys(MapFilter.filters[newValue].$subFilters).length > 0) {
            this.setState({mainFilter: newValue});
            return;
        }
        screenState.setFilter(newValue, null);
    }

    render() {
        let styles = {
            filterPopup: {
                padding: '1rem'
            }
        }
        return (
            <div style={styles.filterPopup}>
                <SelectField value={this.state.mainFilter} onChange={this.onMainFilterChange}>
                    {this.getMainFilterItems()}
                </SelectField>
                <br/>
                {MapFilter.filters[this.state.mainFilter] && MapFilter.filters[this.state.mainFilter].$subFilters && Object.keys(MapFilter.filters[this.state.mainFilter].$subFilters).length > 0 &&
                <RadioButtonGroup name="subfilter" defaultSelected={this.state.subFilter}
                                  onChange={this.onSubfilterChange}>
                    {this.getSubFilterItems()}
                </RadioButtonGroup>
                }
            </div>
        )
    }

    getMainFilterItems() {
        return Object.keys(MapFilter.filters).map((filterId) => {
            return (
                <MenuItem key={filterId} value={filterId} primaryText={I18n.t(MapFilter.filters[filterId].$title)}/>
            );
        })
    }

    getSubFilterItems() {
        return Object.keys(MapFilter.filters[this.state.mainFilter].$subFilters).map((filterId) => {
            return <RadioButton key={filterId} value={filterId}
                                label={I18n.t(MapFilter.filters[this.state.mainFilter].$subFilters[filterId].$title)}/>
        })
    }
}