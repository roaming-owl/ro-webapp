import React from 'react';
import AppBar from './../../components/appBar';
import SearchIcon from 'material-ui/svg-icons/action/search';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import mapFilter from "../../core/mapFilter";
import appTheme from './../../app/theme';
import muiThemeable from 'material-ui/styles/muiThemeable';
import DownArrowIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import {RadioButton, Menu, MenuItem, Popover, SelectField} from "material-ui";
import DadatbaseManager from './../../core/dataManager';
import MainFilterSelect from './mainFilterSelect';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import screenState from './../../state/mapScreen';
import events from './../../core/events';
import MapFilter from './../../core/mapFilter';
import {white, darkBlack} from 'material-ui/styles/colors';
import I18n from './../../core/i18n';

const whiteMuiTheme = Object.assign({}, appTheme, {
    appBar: Object.assign({}, appTheme.appBar, {
        color: white,
        textColor: darkBlack,
    })
});


export default muiThemeable()(class MapAppBar extends AppBar {
    constructor(props) {
        super(props);
        this.state = {
            theme: appTheme,
            filterOpened: false,
            anchorEl: null,
            markersCount: null,
            plant: null,
            article: null,
            showPlantDetail: null,
            ...screenState.state
        }
    }

    componentDidMount() {
        screenState.on(events.STATE.CHANGED, this.onScreenStateChange);
    }

    componentWillUnmount() {
        screenState.off(events.STATE.CHANGED, this.onScreenStateChange);
    }

    onScreenStateChange = (newState) => {
        let filterOpened = this.state.filterOpened;
        if (newState.mainFilter !== this.state.mainFilter &&
            !MapFilter.filters[newState.mainFilter].$subFilters) {
            filterOpened = false;
        }
        if (newState.subFilter !== this.state.subFilter) {
            filterOpened = false;
        }
        let markersCount = null;
        if (newState.markers && newState.markers.length > 0) {
            markersCount = newState.markers.length;
        }
        //TODO: merge two ifs bellow
        if (newState.showPlantDetail !== this.state.showPlantDetail && newState.showPlantDetail !== null) {
            DadatbaseManager.databases['ro_webapp_plants'].getById(newState.showPlantDetail)
            .then((plant) => {
                this.setState({plant: plant, theme: whiteMuiTheme});
            })
        }
        if (newState.showArticleDetail !== this.state.showArticleDetail && newState.showArticleDetail !== null) {
            DadatbaseManager.databases['ro_webapp_articles'].getById(newState.showArticleDetail)
            .then((article) => {
                this.setState({article: article, theme: whiteMuiTheme});
            })
        }
        this.setState({...newState, filterOpened: filterOpened, markersCount: markersCount, plant: null, theme: appTheme});
    }

    handlefilterClose = () => {
        this.setState({
            filterOpened: false,
        });
    }

    handlefilterOpen = (event) => {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            filterOpened: true,
            anchorEl: event.currentTarget,
        });
    };

    getTitle() {
        let styles = {
            appBar: {
                display: 'flex',
                alignItems: 'center',
                height: this.props.muiTheme.appBar.height
            },
            filter: {
                display: 'flex',
                flexDirection: 'column'
            },
            mainFilter: {
                textTransform: 'uppercase',
                lineHeight: this.state.subFilter ? '.8em' : '.9em',
                fontWeight: '300',
                fontSize: this.state.subFilter ? '0.7em' : '0.8em',
                marginBottom: this.state.subFilter ? '0.3em' : 0,
                display: 'flex',
                alignItems: 'center'

            },
            subFilter: {
                lineHeight: '.9em',
                // fontWeight: 'bold',
                fontSize: '.9em',
                display: 'flex',
                alignItems: 'center'
            },
            downIcon: {
                height: this.state.subFilter ? '.9em' : '1em',
            },
            filterButton: {
                height: 50,
                textAlign: 'left'
            },
            filterPopup: {
                padding: '1rem'
            }
        }


        return (
            <div style={styles.appBar}>
                {!this.state.showPlantDetail &&
                <FlatButton onClick={this.handlefilterOpen} style={styles.filterButton}>
                    <div style={styles.filter}>
                        <div style={styles.mainFilter}>
                            <span>{this.getMainFilterName()}</span>
                            {!this.state.subFilter &&
                            <span style={styles.downIcon}><DownArrowIcon/></span>
                            }
                        </div>
                        {this.state.subFilter &&
                        <span style={styles.subFilter}>
                                <span>{I18n.t(MapFilter.filters[this.state.mainFilter].$subFilters[this.state.subFilter].$title)}</span>
                                <span style={styles.downIcon}><DownArrowIcon/></span>
                            </span>
                        }
                    </div>
                </FlatButton>
                }
                {!this.state.showPlantDetail &&
                <Popover
                    open={this.state.filterOpened}
                    anchorEl={this.state.anchorEl}
                    onRequestClose={this.handlefilterClose}
                >
                    <MainFilterSelect/>
                </Popover>
                }
                {this.state.showPlantDetail &&
                <div>{this.state.plant && this.state.plant.name}</div>
                }
            </div>
        )
    }

    hideDetail = () => {
        if (this.state.showPlantDetail) {
            screenState.hidePlantDetail();
        }
        if (this.state.showArticleDetail) {
            screenState.hideArticleDetail();
        }
    }

    getMainFilterName() {
        let title = I18n.t(MapFilter.filters[this.state.mainFilter].$title);
        if (this.state.markersCount > 0) {
            title += ' (' + this.state.markersCount + ')';
        }
        return title;
    }

    getIconElementLeft() {
        if (this.state.showPlantDetail || this.state.showArticleDetail) {
            return (<IconButton onClick={this.hideDetail}><ArrowBack/></IconButton>);
        }
        return super.getIconElementLeft();
    }
});