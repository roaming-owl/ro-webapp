import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import styled from 'styled-components';
import DadatbaseManager from './../../core/dataManager';
import debug from 'debug';
import TextBlock from './popup/textBlock';
import InfoBlock from './popup/infoBlock';
import GalleryBlock from './popup/galleryblock';
import GalleryImage from './popup/galleryimage';
import ReactDOM from 'react-dom';
import domToReact from 'html-react-parser/lib/dom-to-react';

var Parser = require('html-react-parser');

const log = debug('ro-webapp:popup-window')

const PopupDiv = styled.div`
    height: calc(100vh - ${(props) => props.appBarHeight}px);
    width: 100%;
    background-color: white;
    z-index: 1099;
    position: absolute;
    transform: translate3d(0,${(props) => props.translateY},0);
    transition: .3s ease-in-out;
    left: 0;
    top: 0;
`

const LoadingDiv = styled.div`
    display: flex;
    width: 100%;
    height: calc(100vh - ${(props) => props.appBarHeight}px);
    align-items: center;
    justify-content: center;
`

const parserOptions = {
    replace: (domNode) => {
        // do not replace if element has no attributes
        if (!domNode.attribs) return;
        //console.log(domNode);
        if (domNode.name === 'textblock') {
            return (
                <TextBlock>
                    {domToReact(domNode.children, {})}
                </TextBlock>
            )
        }
        if (domNode.name === 'galleryblock') {
            return (
                <GalleryBlock displayVerticalOffset={domNode.attribs['display-vertical-offset']}>
                    {domNode.children.map((node, idx) => {
                        return (
                            <GalleryImage key={idx} imageId={node.attribs['image-id']}
                                          displayVerticalOffset={node.attribs['display-vertical-offset']}/>
                        )
                    })}
                </GalleryBlock>
            )
        }
        if (domNode.name === 'infoblock') {
            let plantInfo = JSON.parse(domNode.attribs.plant);
            return (
                <InfoBlock plant={plantInfo}/>
            )
        }
        // if (domNode.attribs.id === 'main') {
        //     return (
        //         <span style={{ fontSize: '42px' }}>
        //             {domToReact(domNode.children, options)}
        //         </span>
        //     );
        //
        // } else if (domNode.attribs.class === 'prettify') {
        //     return (
        //         <span style={{ color: 'hotpink' }}>
        //             {domToReact(domNode.children, options)}
        //         </span>
        //     );
        // }
    }
};

export default muiThemeable()(class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            plant: null,
            article: null
        }
    }

    // async componentWillReceiveProps(newProps) {
    //     log('New props. loading plant ', newProps.plantId);
    //     if (newProps.plantId) {
    //         let plant = await DadatbaseManager.databases['ro_webapp_plants'].getById(newProps.plantId);
    //         log('Got plant 1', plant);
    //         this.setState({plant: plant});
    //     }
    // }

    async componentWillReceiveProps(newProps) {
        if (newProps.plantId) {
            let plant = await DadatbaseManager.databases['ro_webapp_plants'].getById(newProps.plantId);
            ReactDOM.render(Parser(plant.blocks.cs, parserOptions), this.content);
            this.setState({plant: plant});
        } else {
            this.setState({plant: null});
        }
        if (newProps.articleId) {
            let article = await DadatbaseManager.databases['ro_webapp_articles'].getById(newProps.articleId);
            ReactDOM.render(Parser(article.blocks.cs, parserOptions), this.content);
            this.setState({article: article});
        } else {
            this.setState({article: null});
        }
    }

    // async componentWillMount() {
    //     if (this.props.plantId) {
    //         let plant = await DadatbaseManager.databases['ro_webapp_plants'].getById(this.props.plantId);
    //         log('Got plant 2', plant);
    //         //this.content.innerHTML = plant.blocks.cs;
    //         // this.content.querySelector('text-block').map((element) => {
    //         //     ReactDOM.render()
    //         // });
    //         ReactDOM.render(Parser(plant.blocks.cs, parserOptions), this.content);
    //         this.setState({plant: plant});
    //     }
    // }

    //dangerouslySetInnerHTML={{__html: this.state.plant.blocks.cs}

    render() {
        let styles = {
            centered: {
                margin: '0 auto 0 auto',
                maxWidth: '85%'
            }
        }
        return (
            <PopupDiv translateY={(this.state.plant||this.state.article)?0:'100vh'} appBarHeight={this.props.muiTheme.appBar.height}>
                {(!this.state.plant && !this.state.article) &&
                <LoadingDiv appBarHeight={this.props.muiTheme.appBar.height}>
                    loading...
                </LoadingDiv>
                }
                <div style={{display: (this.state.plant || this.state.article) ? 'block' : 'none'}}>
                    <div style={styles.centered}>
                    <div ref={(content) => {
                        this.content = content;
                    }}/>
                    </div>
                </div>
            </PopupDiv>
        )
    }
});