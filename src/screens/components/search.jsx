import React from 'react';
import screenInstance from './../search';
import BaseScreen from './base';
import IconButton from 'material-ui/IconButton';
import FilterIcon from 'material-ui/svg-icons/content/filter-list';
import muiThemeable from 'material-ui/styles/muiThemeable';

import dataManager from './../../core/dataManager';
import appState from './../../state/app';
import events from './../../core/events';
import search from './../../core/search';
import ResultsList from "./search/resultsList";
import PlantsList from "./search/plantsList";

export default muiThemeable()(class extends BaseScreen {
    constructor(props) {
        super(props);
        this.searchIndexDb = dataManager.databases[search.SEARCH_IN_DATABASES[appState.state.searchIn]];
        this.state = {
            rows: [],
            searchIn: appState.state.searchIn
        }
    }

    getScreenInstance() {
        return screenInstance;
    }

    componentDidMount() {
        appState.setActiveScreen(this.getScreenInstance());
        appState.on(events.SEARCH.CHANGED, this.search);
        dataManager.on(events.DATABASES.SYNC_COMPLETE, this.search);
        if (!dataManager.syncing) {
            this.search();
        }
    }

    componentWillUnmount() {
        appState.off(events.SEARCH.CHANGED, this.search);
        dataManager.off(events.DATABASES.SYNC_COMPLETE, this.search);
    }

    search = () => {
        this.searchIndexDb = dataManager.databases[search.SEARCH_IN_DATABASES[appState.state.searchIn]];
        this.searchIndexDb.search(appState.state.searchFilter)
        .then((rows) => {
            this.setState({rows: rows, searchIn: appState.state.searchIn});
        })
    }

    render() {
        let styles = {
            searchResultsBar: {
                background: '#E1E2E1',
                height: this.props.muiTheme.appBar.height,
                display: 'flex',
                alignItems: 'center',
                paddingLeft: 24,
                paddingRight: 8,
            },
            resultsCount: {
                width: '100%'
            },
            layout: {
                display: 'flex',
                flexDirection: 'column',
                height: 'calc(100vh - ' + this.props.muiTheme.appBar.height + 'px)'
            },

        };
        return (
            <div style={styles.layout}>
                <div style={styles.searchResultsBar}>
                    <div style={styles.resultsCount}>Plants: {this.state.rows && this.state.rows.length}</div>
                    <IconButton><FilterIcon/></IconButton>
                </div>
                {appState.state.searchIn === search.SEARCH_IN.PLANTS &&
                    <PlantsList rows={this.state.rows}/>
                }
            </div>
        )
    }
})