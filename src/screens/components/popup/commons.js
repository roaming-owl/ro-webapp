import styled from 'styled-components';

const BlockDiv = styled.div`
    padding: 1em;
    width: 100%;
    background-color: ${(props) => props.backgroundColor};   
`

export {BlockDiv}