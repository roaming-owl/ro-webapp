import React from 'react';
import {Carousel} from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import {BlockDiv} from './commons';


export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <BlockDiv>
                {this.props.children.length > 1 ?
                    <Carousel emulateTouch={true} showThumbs={false} swipeScrollTolerance={5} width="100%">
                        {this.props.children}
                    </Carousel>
                    :
                    this.props.children
                }
            </BlockDiv>
        )
    }
}