import React from 'react';
import urlUtil from './../../../core/url';

export default function(props) {
    let styles = {
        imgWrapper: {
            width: '100%',
        },
        image: {
            width: '100%',
            userSelect: 'none',
            height: '60vh',
            objectFit: 'cover',
            objectPosition: '50% 50%'
        }
        // objectFit: 'cover',
        // objectPosition: '50% 50%',
        // backgroundImage: `url(${urlUtil.getImageUrl(props.imageId)})`,
        // backgroundRepeat: 'no-repeat'
    };
    return (
        <div style={styles.imgWrapper}>
            <img style={styles.image} src={urlUtil.getImageUrl(props.imageId)} />
        </div>
    )
}