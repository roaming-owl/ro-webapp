import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import appTheme from './../../../app/theme';
import styled from 'styled-components';
import {BlockDiv} from './commons';

const InfoHeader = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;    
`

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            plant: props.plant,
            expanded: false
        }
    }

    toggleMore = () => {
        this.setState({expanded: !this.state.expanded});
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={appTheme}>
                <BlockDiv backgroundColor={'beige'}>
                    <InfoHeader>
                        {this.state.plant.family_name ?
                            <div>{this.state.plant.family_name} ({this.state.plant.family_name_lat})</div>
                            :
                            <div>{this.state.plant.family_name_lat}</div>
                        }
                        <FlatButton label="show more" onClick={this.toggleMore}/>
                    </InfoHeader>
                    {this.state.expanded &&
                    <div>
                        Latin name: {this.state.plant.name_lat}<br/>
                        {this.state.plant.name_en &&
                        <div>English name: {this.state.plant.name_en}<br/></div>
                        }
                        {this.state.plant.name_de &&
                        <div>German name: {this.state.plant.name_de}<br/></div>
                        }
                        {this.state.plant.name_sk &&
                        <div>Slovak name: {this.state.plant.name_de}<br/></div>
                        }
                        {this.state.plant.occurrence &&
                        <div>
                            <br/>
                            Occurence: {this.state.plant.occurrence}
                        </div>
                        }
                    </div>
                    }
                </BlockDiv>
            </MuiThemeProvider>
        )
    }
}