import React from 'react';
import {BlockDiv} from './commons';

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <BlockDiv>
                {this.props.children}
            </BlockDiv>)
    }
}