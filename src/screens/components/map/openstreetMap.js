import React from 'react';
import ReactDom from 'react-dom';
import L from 'leaflet';
import markerCluster from './../../../../node_modules/leaflet.markercluster/dist/leaflet.markercluster-src';
import leafletCss from 'leaflet/dist/leaflet.css';
import extraSmallRedMarkerIcon from './../../../components/markers/extraSmallplantMarker'
import mainLayer from './layerSettings/main';
import baseLayer from './layerSettings/base';
import debug from 'debug';
import './../../../../node_modules/leaflet.markercluster/dist/MarkerCluster.Default.css';
import './../../../../node_modules/leaflet.markercluster/dist/MarkerCluster.css';
import IconBuilder from './icons/builder';
import PlantPopup from './popup/plantPopup';
import ExpositionPopup from './popup/expositionPopup';

const customMarker = L.Marker.extend({
    options: {
        data: null,
    }
});

const log = debug('ro-webapp:osm');

export class OpenstreetMap extends React.Component {

    constructor(props) {
        super(props);
        this.popupElements = [];

        this.map = null;
        this.markersCluster = null;
        this.markers = [];
        this.state = {}
    }

    initLayers() {
        L.tileLayer(baseLayer.url, baseLayer.options).addTo(this.map);
        L.tileLayer(mainLayer.url, mainLayer.options).addTo(this.map);
    }

    componentWillReceiveProps(newProps) {
        this.markersCluster.clearLayers();
        this.initMarkers(newProps.markers);
    }

    createPopup(marker, mapMarker) {
        let popup = L.popup({
            keepInView: true,
            autoPan: true,
            closeButton: false,
            minWidth: 250,
            maxWidth: 250
        })
        popup.setContent(() => {
            let popupContent = document.createElement('div');
            popupContent.classList.add("map-review-popup");
            popupContent.innerHTML = 'Loading...';
            console.log(marker);
            if (marker.type === 'plant') {
                ReactDom.render(<PlantPopup onClose={() => {
                    this.map.closePopup();
                }} onReadMore={() => {
                    this.map.closePopup();
                    /*open overlay*/
                }} marker={marker}/>, popupContent, () => {
                    this.map.panTo(mapMarker.getLatLng());
                });
            } else if (marker.type === 'exposition') {
                ReactDom.render(<ExpositionPopup onClose={() => {
                    this.map.closePopup()
                }} onReadMore={() => {
                    this.map.closePopup();
                    /*open overlay*/
                }} marker={marker}/>, popupContent, () => {
                    // popup.update();
                    this.map.panTo(mapMarker.getLatLng());
                });
            }
            this.popupElements.push(popupContent);
            return popupContent;
        });
        return popup;
    }

    clearPrevious() {
        this.popupElements.map((element) => {
            ReactDom.unmountComponentAtNode(element);
            if (element.parentNode) {
                element.parentNode.removeChild(element);
            }
        });
        this.popupElements = [];
    }

    initMarkers(markersData) {
        this.clearPrevious();
        this.markers = [];
        markersData.map((marker) => {
            let position = [marker.latitude, marker.longitude];
            let mapMarker = new customMarker(position, {
                icon: IconBuilder.buildFrom(marker)
                // new FlowerIcon({
                // iconSize: size, iconAnchor: [size[0] / 2, size[1]],
                // data: marker
                // })
            });
            mapMarker.bindPopup(this.createPopup(marker, mapMarker));
            this.markers.push(mapMarker)
        })
        this.markersCluster.addLayers(this.markers);
    }

    componentDidMount() {
        this.markersCluster = L.markerClusterGroup();
        this.map = L.map(this.props.id, {maxZoom: this.props.maxZoom}).setView([this.props.defaultLatitude, this.props.defaultLongitude], this.props.defaultZoom);
        this.markersCluster.addTo(this.map);
        Promise.all([
            this.initLayers(),
            this.initMarkers(this.props.markers)
        ])
    }

    render() {
        let styles = {
            map: Object.assign({}, {
                height: '100%',
                width: '100%',
                position: 'relative',
                overflow: 'hidden'
            }, this.props.style),
        }
        return (
            <div style={styles.map} id={this.props.id}/>
        )

    }
}



