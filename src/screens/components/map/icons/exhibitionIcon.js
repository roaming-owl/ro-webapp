import L from 'leaflet';
import exhibition from './exhibition.svg';
import svgImage from './marker.svg';
import './expositionIcon.css';

export default L.DivIcon.extend({
    options: {
        className: 'leaflet-exhibition-icon',
        html: `
               <div class="marker">
                   <svg viewBox="${svgImage.viewBox}">
                        <use xlink:href="#${svgImage.id}"/>
                   </svg>
                   <div class="marker-bg"></div>
               </div>               
               <div class="exhibition">
                    <svg viewBox="${exhibition.viewBox}">
                        <use xlink:href="#${exhibition.id}"/>
                    </svg>
               </div>
`
    }
});