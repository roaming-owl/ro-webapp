import FlowerIcon from './plantIcon';
import BloomingFlowerIcon from './bloomingPlantIcon';
import BearingFruitsPlantIcon from './bearingFruitsPlantIcon';
import ExhibitionIcon from './exhibitionIcon';

const hasTag = function (marker, tagCode) {
    if (!marker.tags) {
        return false;
    }
    for (let i = 0; i < marker.tags.length; i++) {
        if (marker.tags[i].code === tagCode) {
            return true;
        }
    }
    return false;
}

export default class {
    static buildFrom(marker) {
        let size = [64, 64];
        if (marker.type === 'exposition') {
            return new ExhibitionIcon({
                iconSize: size, iconAnchor: [size[0] / 2, size[1]],
                data: marker
            });
        }
        if (marker.type === 'plant' && hasTag(marker, 'prave_kvete')) {
            return new BloomingFlowerIcon({
                iconSize: size, iconAnchor: [size[0] / 2, size[1]],
                data: marker
            })
        }
        if (marker.type === 'plant' && hasTag(marker, 'prave_plodi')) {
            return new BearingFruitsPlantIcon({
                iconSize: size, iconAnchor: [size[0] / 2, size[1]],
                data: marker
            })
        }
        return new FlowerIcon({
            iconSize: size, iconAnchor: [size[0] / 2, size[1]],
            data: marker
        })
    }
}