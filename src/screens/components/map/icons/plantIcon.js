import L from 'leaflet';
import flower from './flower.svg';
import './plantIcon.css';
import svgImage from './marker.svg';

export default L.DivIcon.extend({
    options: {
        className: 'leaflet-flower-icon',
        html: `
               <div class="marker">
                   <svg viewBox="${svgImage.viewBox}">
                        <use xlink:href="#${svgImage.id}"/>
                   </svg>
                   <div class="marker-bg"></div>
               </div>
               
               <div class="flower">
                    <svg viewBox="${flower.viewBox}">
                        <use xlink:href="#${flower.id}"/>
                    </svg>
               </div>
`
    }
});
