import appState from './../../../../state/app';
import AppSettings from 'ro-webapp-backend/lib/models/app/settingsNames';


const tilesUrl = appState.state.settings[AppSettings.MAIN_LAYER_TILES_URL].replace(/\$/, '') + '/{z}/{x}/{y}.' + (appState.state.webp ? 'webp' : 'png');

export default {
    url: tilesUrl,
    options: {
        minZoom: 8,
        maxNativeZoom: 21,
        maxZoom: 25,
        attribution: appState.state.settings[AppSettings.MAP_COPYRIGHT]
    }
}