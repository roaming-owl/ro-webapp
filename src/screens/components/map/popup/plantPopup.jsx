import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import appTheme from './../../../../app/theme';
import DadatbaseManager from './../../../../core/dataManager';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import debug from 'debug';
import Url from './../../../../core/url';
import flower from './../icons/flower.svg';
import fruit from './../icons/fruit.svg';
import screenState from './../../../../state/mapScreen';

const log = debug('ro-webapp:plant-popup')

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            marker: props.marker,
            plant: null
        }
    }

    getPlantStyle() {
        if (!this.state.plant) {
            return {};
        }
        return {
            backgroundImage: `url(${Url.getImageUrl(this.state.plant.images[0].id)})`,
            width: '100%',
            height: '250px',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            position: 'relative',
            overflow: 'hidden'
        }
    }

    showPlant = () => {
        //this.props.marker
        screenState.showPlantDetail(this.state.marker.plant_id);
    }

    render() {
        let styles = {
            actionButtons: {
                margin: '1em',
                display: 'flex',
                justifyContent: 'space-between'
            },
            popupBlock: {
                marginLeft: '1em',
                marginRight: '1em',
            },
            header: {
                display: 'flex',
                justifyContent: 'space-between'
            },
            icons: {
                display: 'flex',
                padding: '4px',
                position: 'absolute',
                bottom: 0,
                left: 0,
                height: '32px',
                width: '100%',
                alignItems: 'center',
                backgroundColor: 'rgba(255,255,255,0.7)'
            }
        };
        return (
            <MuiThemeProvider muiTheme={appTheme}>
                <div className='plant-popup'>
                    <div style={styles.header}>
                        <h2 style={styles.popupBlock}>{this.props.marker.plant_name}</h2>
                    </div>
                    {this.state.plant && this.state.plant.images && this.state.plant.images.length > 0 &&
                    <div style={this.getPlantStyle()}>
                        <div style={styles.icons}>
                            <div style={{width: 24, height: 24, margin: 4}}>
                                <svg viewBox={flower.viewBox}>
                                    <use xlinkHref={`#${flower.id}`}/>
                                </svg>
                            </div>
                            <div style={{width: 24, height: 24, margin: 4}}>
                                <svg viewBox={fruit.viewBox}>
                                    <use xlinkHref={`#${fruit.id}`}/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    }
                    <div style={styles.actionButtons}>
                        <FlatButton onClick={this.props.onClose} label='close'/>
                        <RaisedButton primary={true} onClick={this.showPlant} label='read more...'/>
                    </div>
                </div>
            </MuiThemeProvider>);
    }

    async componentDidMount() {
        let plant = await DadatbaseManager.databases['ro_webapp_plants'].getById(this.state.marker.plant_id);
        log('Got plant', plant);
        this.setState({plant: plant});
    }
}