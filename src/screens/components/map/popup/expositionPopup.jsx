import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import appTheme from './../../../../app/theme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DadatbaseManager from './../../../../core/dataManager';
import flower from './../icons/flower.svg';
import fruit from './../icons/fruit.svg';
import debug from 'debug';
import Url from './../../../../core/url';
import screenState from './../../../../state/mapScreen';

//TODO: rename to articlePopup

const log = debug('ro-webapp:article-popup');

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            marker: props.marker,
            article: null
        }
    }

    showArticle = () => {
        screenState.showArticleDetail(this.state.marker.article_id);
    }

    getPlantStyle() {
        if (!this.state.article) {
            return {};
        }
        return {
            backgroundImage: `url(${Url.getImageUrl(this.state.article.images[0].id)})`,
            width: '100%',
            height: '250px',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            position: 'relative',
            overflow: 'hidden'
        }
    }

    render() {
        let styles = {
            actionButtons: {
                margin: '1em',
                display: 'flex',
                justifyContent: 'space-between'
            },
            popupBlock: {
                marginLeft: '1em',
                marginRight: '1em',
            },
            header: {
                display: 'flex',
                justifyContent: 'space-between'
            },
            icons: {
                display: 'flex',
                padding: '4px',
                position: 'absolute',
                bottom: 0,
                left: 0,
                height: '32px',
                width: '100%',
                alignItems: 'center',
                backgroundColor: 'rgba(255,255,255,0.7)'
            }
        };
        return (
            <MuiThemeProvider muiTheme={appTheme}>
                <div className='article-popup'>
                    <div style={styles.header}>
                        <h2 style={styles.popupBlock}>{this.props.marker.title}</h2>
                    </div>
                    {this.state.article && this.state.article.images && this.state.article.images.length > 0 &&
                    <div style={this.getPlantStyle()}>
                        <div style={styles.icons}>
                            <div style={{width: 24, height: 24, margin: 4}}>
                                <svg viewBox={flower.viewBox}>
                                    <use xlinkHref={`#${flower.id}`}/>
                                </svg>
                            </div>
                            <div style={{width: 24, height: 24, margin: 4}}>
                                <svg viewBox={fruit.viewBox}>
                                    <use xlinkHref={`#${fruit.id}`}/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    }
                    <div style={styles.actionButtons}>
                        <FlatButton onClick={this.props.onClose} label='close'/>
                        <RaisedButton primary={true} onClick={this.showArticle} label='read more...'/>
                    </div>
                </div>
            </MuiThemeProvider>);
    }

    async componentDidMount() {
        let article = await DadatbaseManager.databases['ro_webapp_articles'].getById(this.state.marker.article_id);
        log('Got exposition', article);
        this.setState({article: article});
    }
}