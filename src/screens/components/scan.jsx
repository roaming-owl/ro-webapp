import React from 'react';
import screenInstance from './../scan';
import BaseScreen from './base';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Instascan from 'instascan';
import debug from 'debug'
import {Dialog, FloatingActionButton, Snackbar} from "material-ui";
import SwitchCamera from 'material-ui/svg-icons/image/switch-camera';
import UnsupportedIcon from 'material-ui/svg-icons/notification/do-not-disturb'
import screenState from './../../state/scanScreen';
import events from './../../core/events';

const jsQR = require("jsqr");
const log = debug('ro-webapp:scan');

export default muiThemeable()(class extends BaseScreen {
    constructor(props) {
        super(props);

        this.scanner = null;
        this.state = {
            activeCameraIndex: null,
            cameras: null,
            scanHistoryDialogVisible: false
        }
    }

    /**
     * @override
     */
    getScreenInstance() {
        return screenInstance;
    }

    onScanComplete = (content) => {
        //TODO:
        log('Scan complete', content);
    }

    getBackCameraIndex(cameras) {
        for (let i = 0; i < cameras.length; i++) {
            if (cameras[i].name && cameras[i].name.indexOf('back') > -1) {
                return i;
            }
        }
        return null;
    }

    async componentDidMount() {
        try {
            let info = await navigator.mediaDevices.getUserMedia({audio: false, video: true})
            log('video info', info);

        } catch (e) {
            log('Error media', e);
        }
        this.scanner = new Instascan.Scanner({video: document.getElementById('scan-preview')});
        this.scanner.addListener('scan', this.onScanComplete);
        //TODO: use constant
        screenState.on(events.STATE.CHANGED, this.onScreenStateChange);
        this.detectCameras();
    }

    onScreenStateChange = (newState) => {
        console.log('ssc', newState);
        this.setState({scanHistoryDialogVisible: newState.scanHistoryDialogVisible});
    }

    async detectCameras() {
        let context = {};
        return Instascan.Camera.getCameras()
        .then((cameras) => {
            if (!cameras || cameras.length === 0) {
                throw new Error('no cameras found');
            }
            let activeCameraIndex = cameras.length > 1 ? 1 : 0;
            let backCameraIndex = this.getBackCameraIndex(cameras);
            if (backCameraIndex) {
                activeCameraIndex = backCameraIndex;
            }
            context.activeCameraIndex = activeCameraIndex;
            context.cameras = cameras;
            return cameras[activeCameraIndex];
        })
        .then((camera) => {
            return this.scanner.start(camera);
        })
        .then(() => {
            this.setState({activeCameraIndex: context.activeCameraIndex, cameras: context.cameras});
        })
        .catch((e) => {
            console.error('Unable to detect cameras', e);
            this.setState({cameras: []});
        });
    }

    componentWillUnmount() {
        screenState.off(events.STATE.CHANGED, this.onScreenStateChange);
        this.scanner.removeListener('scan', this.onScanComplete);
        this.scanner.stop();
    }

    switchCamera = () => {
        let cameraIndex = this.state.activeCameraIndex;
        cameraIndex++;
        if ((this.state.activeCameraIndex + 1) >= this.state.cameras.length) {
            cameraIndex = 0;
        }
        this.scanner.stop()
        this.scanner.start(this.state.cameras[cameraIndex])
        .then(() => {
            this.setState({activeCameraIndex: cameraIndex});
        });
    }

    hideCameraName = () => {
        this.setState({cameraNameVisible: false});
    }

    handleScanHistoryDialogClose = () => {
        screenState.hideHistoryDialog();
    }

    render() {
        let styles = {
            videoPreview: {
                width: '100%',
                height: 'calc(100vh - ' + this.props.muiTheme.appBar.height + 'px)',
                objectFit: 'cover'
            },
            videoWrapper: {
                position: 'relative',
                //opacity: (this.state.cameras && this.state.cameras.length > 0) ? 1 : 0
                display: (this.state.cameras && this.state.cameras.length > 0) ? 'block' : 'none',
                overflow: 'hidden'
            },
            switchCameraButton: {
                position: 'absolute',
                bottom: '3rem',
                right: '1rem'
            },
            cameraName: {
                color: 'white',
                textAlign: 'center'
            },
            screenWrapper: {
                width: '100%',
                height: 'calc(100vh - ' + this.props.muiTheme.appBar.height + 'px)',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center'
            },
            noCameras: {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            },
            unsupportedIcon: {
                width: '5rem',
                height: '5rem'
            }
        }
        return (
            <div style={styles.screenWrapper}>
                {!this.state.cameras &&
                <div>Searching for cameras...</div>
                }
                {this.state.cameras && this.state.cameras.length === 0 &&
                <div style={styles.noCameras}>
                    <UnsupportedIcon style={styles.unsupportedIcon}/>
                    <div>no cameras found</div>
                </div>
                }
                <div style={styles.videoWrapper}>
                    <video style={styles.videoPreview} id="scan-preview"></video>
                    {this.state.cameras && this.state.cameras.length > 1 &&
                    <FloatingActionButton secondary={true} style={styles.switchCameraButton}>
                        <SwitchCamera onClick={this.switchCamera}/>
                    </FloatingActionButton>
                    }
                </div>
                <Dialog
                    title="Dialog With Actions"
                    modal={false}
                    open={this.state.scanHistoryDialogVisible}
                    onRequestClose={this.handleScanHistoryDialogClose}>
                    hello!
                </Dialog>
            </div>
        )
    }
});