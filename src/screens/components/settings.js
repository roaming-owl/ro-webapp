import React from 'react';
import screenInstance from './../settings';
import BaseScreen from './base';
import translatable from './../../components/translatable';
import appState from './../../state/app';
import {FlatButton} from "material-ui";



export default translatable(class extends BaseScreen {
    getScreenInstance() {
        return screenInstance;
    }

    changeLanguage(locale) {
        appState.setLocale(locale)
    }

    render() {
        return (
            <div>
                T: {this.props.t('settings')}
                <hr/>
                <FlatButton label="en-US" onClick={() => this.changeLanguage('en-US')}></FlatButton>
                <FlatButton label="cs" onClick={() => this.changeLanguage('cs')}></FlatButton>
            </div>
        )
    }
})