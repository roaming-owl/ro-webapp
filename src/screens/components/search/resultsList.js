import React from 'react';
import {AutoSizer, List} from 'react-virtualized';

export default function(props) {
// export default class ResultsList extends React.Component {
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             rows: this.props.rows,
//             searchIn: this.props.searchIn
//         }
//     }
//
//     componentWillReceiveProps(newProps) {
//         this.setState({rows: newProps.rows, searchIn: newProps.searchIn});
//     }
//
//
//     $noRowsRenderer() {
//         return <div>No rows</div>;
//     }

    // $rowRenderer = ({index, isScrolling, key, style}) => {
    //     if (this.state.searchIn === search.SEARCH_IN.PLANTS) {
    //         return (
    //             <div style={style} key={index}>
    //                 <div>{this.state.rows[index].name}</div>
    //                 <div><i>{this.state.rows[index].name_lat}</i></div>
    //             </div>
    //         )
    //     } else if (this.state.searchIn === search.SEARCH_IN.EXPOSITIONS) {
    //         return (
    //             <div style={style} key={index}>
    //                 <div>{this.state.rows[index].title}</div>
    //             </div>
    //         )
    //     }
    // }

    // render() {
        let styles = {
            list: {
                position: 'relative',
                flex: '1 1 auto',
            }
        }
        return (
            <div style={styles.list}>
                <AutoSizer>
                    {({width, height}) =>
                        <List
                            ref="List"
                            height={height}
                            overscanRowCount={40}
                            //noRowsRenderer={this.$noRowsRenderer}
                            noRowsRenderer={props.noRowsComponent}
                            // rowCount={this.state.rows.length}
                            rowCount={props.rows.length}
                            // rows={this.state.rows}
                            rows={props.rows}
                            // rowHeight={60}
                            rowHeight={props.rowHeight}
                            // rowRenderer={this.$rowRenderer}
                            rowRenderer={props.rowComponent}
                            // scrollToIndex={scrollToIndex}
                            className='unfocusable' //chrome was showing ouline on focus
                            width={width}
                        />}
                </AutoSizer>
            </div>
        )
    // }
}