import React from 'react';
import ResultsList from './resultsList';

const PlantsRow = function (props) {
    return (
        <div style={props.style} key={props.index}>
            <div>{props.row.name}</div>
            <div><i>{props.row.name_lat}</i></div>
        </div>
    );
};

const noRowsRenderer = function () {
    return (<div>No rows</div>);
};

export default function (props) {
    return (
        <ResultsList rows={props.rows} rowHeight={60} noRowsComponent={noRowsRenderer}
                     rowComponent={({index, isScrolling, key, style}) => <PlantsRow key={index} index={index}
                                                                                    style={style}
                                                                                    row={props.rows[index]}/>}/>
    );
}