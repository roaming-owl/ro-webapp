import React from 'react';
import screenInstance from './../map';
import BaseScreen from './base';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {OpenstreetMap} from './map/openstreetMap';
import appState from './../../state/app';
import appTheme from './../../app/theme';
import screenState from './../../state/mapScreen';
import settings from 'ro-webapp-backend/lib/models/app/settingsNames';
import debug from 'debug';
import dataManager from './../../core/dataManager';
import markersDb from './../../database/markers'
import events from './../../core/events';
import mapFilter from './../../core/mapFilter';
import PopupWindow from './popupWindow';

const log = debug('ro-webapp:mapScreen');

const MAP_ELEMENT_ID = 'main-map';

export default muiThemeable()(class extends BaseScreen {

    constructor(props) {
        super(props);

        this.map = null;
        this.state = {
            ...screenState.state
        };
    }

    /**
     * @override
     */
    getScreenInstance() {
        return screenInstance;
    }

    componentWillUnmount() {
        screenState.off(events.STATE.CHANGED, this.onScreenStateChange);
        screenState.stopTrackLocation();
    }

    componentDidMount() {
        appState.setActiveScreen(this.getScreenInstance());
        screenState.on(events.STATE.CHANGED, this.onScreenStateChange);
        this.loadMarkers();
        screenState.trackLocation();
    }

    onScreenStateChange = (newState) => {
        //TODO: refactor, ugly!
        if (this.state.mainFilter !== newState.mainFilter || this.state.subFilter !== newState.subFilter) {
            this.setState({...newState}, () => {
                this.loadMarkers();
            });
        }
        this.setState({...newState});
    };

    loadMarkers = () => {
        log('Loading markers...');

        let containsTag = function (tags, code) {
            for (let i = 0; i < tags.length; i++) {
                if (tags[i].code === code) {
                    return true;
                }
            }
            return false;
        }

        let filter = (item) => {
            let result = true;
            if (mapFilter.filters[this.state.mainFilter].type) {
                result = item.type === mapFilter.filters[this.state.mainFilter].type;
            }
            if (this.state.subFilter) {
                let subFilter = mapFilter.filters[this.state.mainFilter].$subFilters[this.state.subFilter];
                if (subFilter.tags && subFilter.tags.$contains) { //todo: generalize
                    subFilter.tags.$contains.map((code) => {
                        result = result && item.tags && containsTag(item.tags, code)
                    })
                }
            }
            return result;
        };
        return dataManager.databases[markersDb.NAME].search(filter)
        .then((markers) => {
            screenState.setMarkers(markers);
        })
    }

    render() {
        console.log('Showing article: ', this.state.showArticleDetail);
        let styles = {
            layout: {
                top: this.props.muiTheme.appBar.height,
                position: 'relative',
                overflow: 'hidden'
            },
            map: {
                height: 'calc(100vh - ' + this.props.muiTheme.appBar.height + 'px)',
            },
            popupWindow: {
                position: 'absolute',
                transform: 'translate3d(0, 100vh, 0)'
            }
        };
        return (
            <div style={styles.layout}>
                <OpenstreetMap id={MAP_ELEMENT_ID}
                               defaultLatitude={appState.state.settings[settings.DEFAULT_MAP_LATITUDE]}
                               defaultLongitude={appState.state.settings[settings.DEFAULT_MAP_LONGITUDE]}
                               defaultZoom={appState.state.settings[settings.DEFAULT_MAP_ZOOM]}
                               maxZoom={appState.state.settings[settings.MAX_MAP_ZOOM]}
                               style={styles.map} markers={this.state.markers}/>
                <PopupWindow plantId={this.state.showPlantDetail} articleId={this.state.showArticleDetail}></PopupWindow>
            </div>
        )
    }
});