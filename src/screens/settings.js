import asyncComponent from './../components/asyncComponent';
import AppBar from './../components/appBar';
import Sidebar from './../components/sidebar';

const AsyncSearchScreen = asyncComponent(() => import('./components/settings'));

const CODE = 'settings';

const screen = {
    code: CODE,
    icon: null,
    content: AsyncSearchScreen,
    appBar: AppBar,
    sidebar: Sidebar.AppSidebar,
    appTitle: CODE,
    menuItem: {
        label: CODE
    }
}

export default screen;