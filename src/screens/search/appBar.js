import React from 'react';
import AppBar from './../../components/appBar';
import {TextField} from "material-ui";
import SearchIcon from 'material-ui/svg-icons/action/search';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {white, darkBlack} from 'material-ui/styles/colors';
import appState from './../../state/app';
import appTheme from './../../app/theme';
import $defaultFilter from './../../core/filter/defaultFilter';

const whiteMuiTheme = Object.assign({}, appTheme, {
    appBar: Object.assign({}, appTheme.appBar, {
        color: white,
        textColor: darkBlack,
    })
});

export default class SearchAppBar extends AppBar {
    constructor(props) {
        super(props);
        this.state = {
            theme: whiteMuiTheme,
            searchVisible: true,
            search: ''
        }
    }

    onSearchChange = (evt, newSearch) => {
        if (newSearch && newSearch.length > 0) {
            newSearch = newSearch.toLowerCase();
        }
        this.setState({search: newSearch});
        appState.setSearchFilter($defaultFilter(newSearch));
    }

    enableSearch = () => {
        this.setState({searchVisible: !this.state.searchVisible, theme: whiteMuiTheme}, () => {
            this.searchTextBoxComponent.focus();
        });
    }

    clearSearch = () => {
        this.setState({search: ''});
        appState.setSearchFilter();
    }

    getTitle() {
        if (this.state.searchVisible) {
            return (<TextField ref={(input) => {
                this.searchTextBoxComponent = input;
            }} fullWidth={true} value={this.state.search} hintText="Search plant"
                               onChange={this.onSearchChange}/>)
        }
        return "All plants";
    }

    getIconElementRight() {
        if (this.state.searchVisible) {
            return (<IconButton onClick={this.clearSearch}><ClearIcon/></IconButton>);
        }
        return (<IconButton onClick={this.enableSearch}><SearchIcon/></IconButton>);
    }
}