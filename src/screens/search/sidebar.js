import React from 'react';
import Sidebar from './../../components/sidebar';
import screenInstance from './../search';
import Divider from 'material-ui/Divider';
import MenuItem from 'material-ui/MenuItem';
import AppState from './../../state/app';
import searchFilter from './../../core/search'
import sidebarState from './../../state/sidebar';
import {withRouter} from 'react-router-dom';

class SearchSidebar extends Sidebar.Sidebar {

    setExpositions = () => {
        sidebarState.close()
        .then(() => {
            AppState.setSearchIn(searchFilter.SEARCH_IN.EXPOSITIONS);
        })
    }

    setPlants = () => {
        sidebarState.close()
        .then(() => {
            AppState.setSearchIn(searchFilter.SEARCH_IN.PLANTS);
        })
    }

    getMenuItemComponent(screen) {
        let menuItem = super.getMenuItemComponent(screen);
        if (screen.code !== screenInstance.code) {
            return menuItem;
        }
        let extendedMenu = [
            <Divider/>,
            menuItem,
            <Divider/>,
            <MenuItem onClick={this.setPlants} primaryText="Rostliny" insetChildren={true}/>,
            <MenuItem primaryText="Zvířata" insetChildren={true}/>,
            <MenuItem onClick={this.setExpositions} primaryText="Expozice" insetChildren={true}/>,
            <MenuItem primaryText="Okruhy" insetChildren={true}/>,
            <MenuItem primaryText="Události" insetChildren={true}/>,
            <MenuItem primaryText="Články" insetChildren={true}/>,
            <Divider/>
        ];
        return extendedMenu;
    }
}

export default withRouter((props) => {
    return <SearchSidebar {...props}/>
})