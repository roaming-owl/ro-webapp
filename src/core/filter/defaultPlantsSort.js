export default function (a, b) {
    var aName = a.name;
    var bName = b.name;
    if (aName < bName) {
        return -1;
    }
    if (aName > bName) {
        return 1
    }
    var aLatName = a.name_lat;
    var bLatName = b.name_lat;
    if (aLatName < bLatName) {
        return -1;
    }
    if (aLatName > bLatName) {
        return 1
    }
    return 0;
};
