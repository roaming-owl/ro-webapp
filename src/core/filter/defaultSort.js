/*
default sort for db rows
id key should be always present
 */
export default function (a, b) {
    if (a.id < b.id) {
        return -1;
    }
    if (a.id > b.id) {
        return 1
    }
    return 0;
};
