import {removeDiacritics} from './../../util/string';

export default function (searchString, searchFields = {}) {
    return function (item) {
        if (searchString && searchString.length > 0 &&
            item.name.toLowerCase().indexOf(searchString) === -1 &&
            item.name_lat.toLowerCase().indexOf(searchString) === -1 &&
            item.name_en.toLowerCase().indexOf(searchString) === -1 &&
            item.name_sk.toLowerCase().indexOf(searchString) === -1 &&
            item.name_de.toLowerCase().indexOf(searchString) === -1 &&
            //todo: do this on server & store it with item!
            removeDiacritics(item.name.toLowerCase()).indexOf(removeDiacritics(searchString)) === -1) {
            return false;
        }
        if (searchFields.family_id && item.family_id !== searchFields.family_id) {
            return false;
        }
        if (searchFields.tag_id && searchFields.tag_id !== '__none__' && !item.tagsList[searchFields.tag_id]) {
            return false;
        }
        return true;
    }
}