import AppState from './../state/app';
import {APP_SETTINGS} from 'ro-webapp-backend';

export default {
    getImageUrl(imageId, size = 640) {
        let url = AppState.state.settings[APP_SETTINGS.IMAGES_URL].replace(/\/$/, '') + '/';
        url += size + '/' + imageId + (AppState.state.webp ? '.webp' : '.png');
        return url;
    }
}