import PlantsSearchIndex from '../database/plantsSearchIndex';
import ExpositionsSearchIndex from '../database/expositionsSearchIndex';
import Markers from '../database/markers';
import Plants from '../database/plants';
import Articles from '../database/articles';
import EventEmitter from 'wolfy87-eventemitter';
import events from './../core/events';
import debug from 'debug';

const log = debug('ro-webapp:db-manger');

class DataManager extends EventEmitter{
    constructor() {
        super();
        this.databases = {};
        this.syncing = false;
    }

    registerDatabase(dbInstance) {
        this.databases[dbInstance.name] = dbInstance
    }

    async sync() {
        this.syncing = true;
        log('Syncong databases: ', Object.keys(this.databases).join(','));
        let queue = Promise.resolve();
        Object.keys(this.databases).map((dbName) => {
            queue = queue.then(() => {
                return this.databases[dbName].sync()
            })
            .then((info) => {
                log(`Db ${dbName} ready. Records count: ${info.docCount}`)
                this.emit(events.DATABASE.SYNC_COMPLETE, dbName);
            })
            .catch((e) => {
                log('Unable to sync db ', dbName, e);
            })
        })
        queue = queue.then(() => {
            this.emit(events.DATABASES.SYNC_COMPLETE, {});
            this.syncing = false;
        })
        return queue;
    }
}

let manager = new DataManager();
manager.registerDatabase(new PlantsSearchIndex.Database());
manager.registerDatabase(new ExpositionsSearchIndex.Database());
manager.registerDatabase(new Markers.Database());
manager.registerDatabase(new Plants.Database());
manager.registerDatabase(new Articles.Database());

export default manager;