export default {
    filters: {
        all: {
            $title: 'All',
        },
        plants: {
            $title: 'Plants',
            type: 'plant',
            $subFilters: { //TODO: maybe there is a better way to define multi level filters?
                '$all': {
                    $title: 'All plants',
                },
                blooming: {
                    $title: 'Blooming',
                    tags: {
                        $contains: ['prave_kvete']
                    }
                },
                /*
                TODO: make plants tags regen script!!!!  in nodejs
                 */
                bearingFruits: {
                    $title: 'Bearing fruits',
                    tags: {
                        $contains: ['prave_plodi']
                    }
                },
            }
        },
        exhibitions: {
            $title: 'Expositions',
            type: 'exposition'
        },
        // animals: {
        //     type: 'plant', //currenlty in plants table
        //     tags: {
        //         $contains: 'animal'
        //     }
        // },
        // animals: {
        //     type: 'plant', //currenlty in plants table
        //     tags: {
        //         $contains: 'animal'
        //     }
        // }
        //TODO: load all this from DB?
    }
}