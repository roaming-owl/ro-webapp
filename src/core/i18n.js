import appStatus from './../state/app';
import events from './../core/events';
import debug from 'debug';
const log = debug('ro-webapp:i18n');

const I18n = require('react-i18nify').I18n;

const dictionaries = {
}

appStatus.on(events.APPLICATION.LANGUAGE_ADDED, (locale) => {
    log(`New language loaded: ${locale}`);
    dictionaries[locale] = appStatus.state.languages[locale];
    I18n.setTranslations(dictionaries);
});

appStatus.on(events.APPLICATION.LANGUAGE_CHANGED, (locale) => {
    log(`Switching language to ${locale}`);
    I18n.setLocale(locale);
});

I18n.setTranslations(dictionaries);

/**
 * @singleton
 */
export default I18n;