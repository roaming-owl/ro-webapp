import debug from 'debug';
const log = debug('ro-webapp:jsonRequest');

export default class {
    constructor(url, options) {
        this.url = url;
        this.options = options;
        this.cancelled = false;
    }

    cancel() {
        this.cancelled = true;
    }

    async fetch() {
        log(`Fetching ${this.url}, options: `, this.options);
        return fetch(this.url, this.options)
        .then((response) => {
            return response.json();
        })
        .then((response) => {
            if (this.cancelled) {
                log(`Request ${this.url} cancelled`);
                return Promise.reject({cancelled: true})
            }
            return response;
        })
    }
}