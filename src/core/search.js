import PlantsSearchIndex from './../database/plantsSearchIndex';
import ExpositionsSearchIndex from './../database/expositionsSearchIndex';

//TODO: wtf is this? maybe could be merged with map filter??
export default {
    SEARCH_IN: {
        PLANTS: 'plants',
        ANIMALS: 'animals',
        EXPOSITIONS: 'expositions',
        TRAILS: 'trails',
        EVENTS: 'events',
        ARTICLES: 'articles'
    },
    SEARCH_IN_DATABASES: {
        plants: PlantsSearchIndex.NAME,
        animals: '',//TODO
        expositions: ExpositionsSearchIndex.NAME,
        trails: '',//TODO
        events: '',//TODO
        articles: '' //TODO
    }
}