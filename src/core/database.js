import PouchDB from 'pouchdb';
import dexie from 'dexie';
import Request from './jsonRequest';
import $matchAllFilter from './filter/matchAll';
import $defaultSort from './filter/defaultSort';
import debug from 'debug';

const log = debug('ro-webapp:database');


//temp
window.PouchDB = PouchDB;

export default class Database {
    constructor(dbName, options) {
        this.name = dbName;
        this.localCahce;
    }

    async getUpdateInfo() {
        let remoteRowsIndex = await this.$fetchRemoteRowsIndex();
        let localRowsIndex = await this.$fetchLocalRowsIndex();
        let localItemIds = Object.keys(localRowsIndex);
        let newItemsIds = [];
        let changedItemIds = [];
        log(`Fetched ${Object.keys(remoteRowsIndex).length} items in remote index`);
        log(`Fetched ${Object.keys(localRowsIndex).length} items in local index`, localRowsIndex);
        Object.keys(remoteRowsIndex).filter((itemId) => {
            if (!localRowsIndex[itemId]) {
                newItemsIds.push(itemId);
            } else {
                if (localRowsIndex[itemId] !== remoteRowsIndex[itemId]) {
                    changedItemIds.push(itemId);
                }
            }
        });
        let deletedItemsIds = [];
        localItemIds.filter((itemId) => {
            if (!remoteRowsIndex[itemId]) {
                deletedItemsIds.push(itemId);
            }
        });
        return {
            needsUpdate: (changedItemIds.length > 0) || (newItemsIds.length > 0) || (deletedItemsIds.length > 0),
            changedItemIds: changedItemIds,
            newItemsIds: newItemsIds,
            deletedItemsIds: deletedItemsIds

        }
    }

    async clearAll() {
        let db = new PouchDB(this.name);
        console.log(`Db ${this.name} cleared`);
        return db.destroy();
    }

    async $fetchAllLocalRows() {
        let db = new PouchDB(this.name);
        let allDocs = await db.allDocs({include_docs: true});
        let docs = [];
        allDocs.rows.map((item) => {
            docs.push(item.doc);
        });
        return docs;
    }


    async $fetchLocalRowsIndex() {
        let db = new PouchDB(this.name);
        let allDocs = await db.allDocs({include_docs: true});
        let index = {};
        if (allDocs.total_rows === 0) {
            return {};
        }
        allDocs.rows.map((item) => {
            index[item.doc.id] = item.doc.updated_at;
        });
        return index;
    }

    async $fetchRemoteRowsIndex() {
        throw new Error('not implemented');
    }

    async $fetchLocalRow(id) {
        let db = new PouchDB(this.name);
        return db.get(id + '')
        .catch((e) => {
            throw e;
        })
    }

    async $fetchRemoteRow(id) {
        throw new Error('not implemented');
    }

    async $getDocCount() {
        let db = new PouchDB(this.name);
        return db.info()
        .then((info) => {
            return info.doc_count;
        })
    }

    async getById(id) {
        try {
            let item = await this.$fetchLocalRow(id + '');
            return item;
        } catch (e) {
            log('Unable to get item from db ', e);
            return null;
        }
    }

    async search(filter = $matchAllFilter, sort = $defaultSort) {
        if (!this.localCahce) {
            this.localCahce = await this.$fetchAllLocalRows();
        }
        //TODO: simple filter!
        return this.localCahce.filter(filter).sort(sort);
    }

    async $addRow(data) {
        log('Add row ', data);
        let db = new PouchDB(this.name);
        try {
            return db.put(Object.assign({}, data, {_id: data.id + ''}));
        } catch (e) {
            if (e.status && e.status === 409) {
                log('put conflict, removing');
                await this.$removeRow(data.id);
                return db.put(Object.assign({}, data, {_id: data.id + ''}));
            }
        }
    }

    async $updateRow(id, data) {
        log('Update row ', id, data);
        let db = new PouchDB(this.name);
        await this.$removeRow(id);
        return this.$addRow(data);
        // let item = await db.get(id + '');
        //
        // return db.put(Object.assign({_id: item._id, _rev: item._rev}, data));
    }

    async $removeRow(id) {
        let db = new PouchDB(this.name);
        //let doc = await this.$fetchRemoteRow(id); //?wtf was this??
        let doc = await db.get(id + '');
        return db.remove(doc);
    }

    async sync() {
        console.log('Syncing ', this.name);
        let updateInfo = await this.getUpdateInfo();
        log('Update info ', updateInfo);
        if (!updateInfo.needsUpdate) {
            let docCount = await this.$getDocCount();
            return {docCount: docCount};
        }
        log(`Updating db, removing old stuff: ${updateInfo.deletedItemsIds.length} items`);
        for (let i = 0; i < updateInfo.deletedItemsIds.length; i++) {
            try {
                log('removing ', updateInfo.deletedItemsIds[i]);
                await this.$removeRow(updateInfo.deletedItemsIds[i]);
            } catch (e) {
                console.error('Unable to remove item ' + updateInfo.deletedItemsIds[i] + 'from db ' + this.name);
            }
        }
        //let toBeDowloaded = updateInfo.needsUpdate.concat(updateInfo.newItemsIds);
        log(`Updating db, adding new stuff: ${updateInfo.newItemsIds.length} items`);
        for (let i = 0; i < updateInfo.newItemsIds.length; i++) {
            let remoteRow = await this.$fetchRemoteRow(updateInfo.newItemsIds[i]);
            await this.$addRow(remoteRow);
        }
        log(`Updating db, updating stuff: ${updateInfo.changedItemIds.length} items`);
        for (let i = 0; i < updateInfo.changedItemIds.length; i++) {
            let remoteRow = await this.$fetchRemoteRow(updateInfo.changedItemIds[i]);
            await this.$updateRow(updateInfo.changedItemIds[i], remoteRow);
        }
        let docCount = await this.$getDocCount();
        return {docCount: docCount};
    }
}