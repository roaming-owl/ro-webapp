import history from './../components/history';

class Router {
    getRouteTo(screenInstance) {
        return '/' + screenInstance.code;
    }

    openScreen(screenInstance) {
        console.log('opening', this.getRouteTo(screenInstance));
        history.push(this.getRouteTo(screenInstance));
    }
}

export default new Router();