export default {
    platform: {
        DESKTOP: 'desktop',
        TABLET: 'tablet',
        MOBILE: 'mobile'
    }
}