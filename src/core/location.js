import EventBus from 'wolfy87-eventemitter';
import debug from 'debug';

const log = debug('ro-webapp:location');

const Location = class extends EventBus {
    constructor() {
        super();
        this.locationWatcherId = null;
    }

    watch() {
        this.locationWatcherId = navigator.geolocation.watchPosition((watchedPos) => {
                this.emit('location-change', watchedPos);
                log('Location watch started');
            }, (err) => {
                //TODO: console error
                navigator.geolocation.clearWatch(this.locationWatcherId);
                log('Unable to watch location ', err);
                throw err;
            }, {enableHighAccuracy: true, timeout: 5000}
        );
    }

    async stopWatching() {
        if (!this.locationWatcherId) {
            return;
        }
        navigator.geolocation.clearWatch(this.locationWatcherId);
        log('Location watch stopped');
    }

    async isBlocked() {
        if (!this.isAvailable()) {
            return true; //??ok
        }
        return new Promise((resolve) => {
            navigator.geolocation.getCurrentPosition(() => {
                log('Location available');
                return resolve(true);
            }, (e) => {
                log('Location NOT available', e);
                if (e.code === e.PERMISSION_DENIED) {
                    return resolve(true);
                }
            }, {enableHighAccuracy: true, timeout: 5000});
        })
    }

    isAvailable() {
        return navigator.geolocation;
    }
}
export default new Location();