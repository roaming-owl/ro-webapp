export default {
    SIDEBAR: {
        OPENED: 'sidebar_opened',
        CLOSED: 'sidebar_closed',
        OPEN_CHANGED: 'sidebar_open_changed',
        OPEN_CHANGING: 'sidebar_open_changing',
    },
    APPLICATION: {
        LANGUAGE_ADDED: 'language_added',
        LANGUAGE_CHANGED: 'language_changed',
    },
    DATABASE: {
        SYNC_COMPLETE: 'sync_complete'
    },
    DATABASES: {
        SYNC_COMPLETE: 'sync_complete'
    },
    SEARCH: {
        CHANGED: 'search_changed',
    },
    STATE: {
        CHANGED: 'state-changed',
    }
}