import appState from './../state/app';
import Request from '../core/jsonRequest';
import bowser from 'bowser';
import debug from 'debug';
const log = debug('ro-webapp:evnironment');

class Environment {
    async isOnline() {
        let isOnline = false;
        try {
            let response = await new Request('/api/v1.0/ping', {method: 'get'}).fetch();
            if (response === 'pong') {
                isOnline = true;
            }
        } catch (e) {
            console.warn('Not online');
        }
        return Promise.resolve(isOnline);
    }

    async isInstalled() {
        if (window.matchMedia('(display-mode: standalone)').matches) {
            log('App is running in standalone mode');
            return Promise.resolve(true);
        }
        log('App is running in browser mode');
        return Promise.resolve(false);
    }

    async webpSupported() {
        return new Promise((resolve) => {
            (function () {
                var img = new Image();
                img.onload = function () {
                    resolve(!!(img.height > 0 && img.width > 0));
                };
                img.onerror = function () {
                    resolve(false);
                };
                img.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';
            })();
        })
    }

    getLocale() {
        var lang = localStorage.getItem('locale');
        if (lang) {
            return lang;
        } else if (navigator.languages && navigator.languages.length) {
            // latest versions of Chrome and Firefox set this correctly
            lang = navigator.languages[0]
        } else if (navigator.userLanguage) {
            // IE only
            lang = navigator.userLanguage;
        } else {
            // latest versions of Chrome, Firefox, and Safari set this correctly
            lang = navigator.language;
        }

        return lang;
    }

    async getAppSettings() {
        return new Request('/api/v1.0/app/settings').fetch()
    }

    async resolveBrowser() {
        return Promise.resolve(bowser);
    }

    async resolveLanguage() {
        let locale = this.getLocale();
        if (locale !== 'en-US' && locale !== 'cs') {
            locale = 'en-US';
        }
        return new Request('/api/v1.0/app/languages/' + locale).fetch()
        .then((languageData) => {
            appState.addLanguage(locale, languageData.dictionary)
            return locale;
        })
        .catch((e) => {
            console.error('Unable to load langauge ', locale);
        })
    }

    async recoginze() {
        let isOnline, webpSupported, isInstalled, browser, settings, locale;
        [isOnline, webpSupported, isInstalled, browser, settings, locale] = [
            await this.isOnline(),
            await this.webpSupported(),
            await this.isInstalled(),
            await this.resolveBrowser(),
            await this.getAppSettings(),
            await this.resolveLanguage()
        ];
        appState.setWebPSupport(webpSupported);
        appState.setOnline(isOnline);
        appState.setInstalled(isInstalled);
        appState.setBrowser(browser);
        appState.setSettings(settings);
        appState.setLocale(locale);
        appState.setLocale(locale);
    }
}

export default new Environment();