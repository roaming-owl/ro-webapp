import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {lightGreen500, lightGreen700, orange500, grey500, grey100, grey400,lightBlack} from 'material-ui/styles/colors';

export default getMuiTheme({
    palette: {
        primary1Color: lightGreen500,
        primary2Color: lightGreen700,
        //primary3Color: grey400,
        // accent1Color: orange500,
        // accent2Color: grey100,
        // accent3Color: grey500,
        alternateTextColor: lightBlack
    }
});