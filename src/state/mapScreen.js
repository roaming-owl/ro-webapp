import Base from './base';
import MapFilter from './../core/mapFilter';
import location from './../core/location';
import debug from 'debug';

const log = debug('ro-webapp:mapScreenState');

class MapScreen extends Base {
    constructor() {
        super();
        this.state = {
            mainFilter: 'plants', //TODO: load from localStorage then default from DB!
            subFilter: '$all',
            markers: [],
            //selectedMmarker??
            showPlantDetail: null,
            showArticleDetail: null,
            location: null
        }
    }

    trackLocation() {
        if (!location.isAvailable()) {
            return;
        }
        location.on('locationUpdate', this.onLocationChange);
        try {
            location.watch();
        } catch (e) {
            log('Unable to watch location ', e);
            location.off('location-update', this.onLocationChange);
        }

    }

    onLocationChange(position) {
        if (!location.isAvailable()) {
            return;
        }
        log('Updating my location', position),
        this.setState({location: location});
    }

    stopTrackingLocation() {
        if (!location.isAvailable()) {
            return;
        }
        location.off('location-update', this.onLocationChange);
        location.stopWatching();
    }

    showArticleDetail(id) {
        log('Showing article: ', id);
        this.setState({showArticleDetail: id});
    }

    hideArticleDetail() {
        if (this.state.showArticleDetail) {
            this.setState({showArticleDetail: null});
        }

    }

    showPlantDetail(id) {
        log('Showing plant: ', id);
        this.setState({showPlantDetail: id});
    }

    hidePlantDetail() {
        if (this.state.showPlantDetail) {
            this.setState({showPlantDetail: null});
        }

    }

    setFilter(mainFilter, subFilter) {
        log('Filter set (main, sub): ', mainFilter, subFilter);
        this.setState({subFilter: subFilter, mainFilter: mainFilter});
    }

    setMarkers(markers) {
        log('markers set: ', markers);
        this.setState({markers: markers});
    }
}

export default new MapScreen();