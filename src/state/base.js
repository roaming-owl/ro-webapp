import EventEmitter from 'wolfy87-eventemitter';
import events from './../core/events';

export default class BaseState extends EventEmitter {
    constructor() {
        super();
        this.state = {};
    }

    setState(values) {
        this.state = Object.freeze(Object.assign({}, this.state, values));
        this.emit(events.STATE.CHANGED, this.state);
    }
}