import Base from './base';

class ScanScreen extends Base {
    constructor() {
        super();
        this.state = {
            scanHistoryDialogVisible: false
        }
    }

    showHistoryDialog() {
        this.setState({scanHistoryDialogVisible: true});
    }

    hideHistoryDialog() {
        this.setState({scanHistoryDialogVisible: false});
    }
}

export default new ScanScreen();