// import Bus from './../core/bus';
import Events from './../core/events';
import Base from './base';

class Sidebar extends Base {
    constructor() {
        super();

        this.state = {  //TODO: local storage
            open: false,
        };
    }

    $emitOpenChanging(changingTo) {
        this.emit(Events.SIDEBAR.OPEN_CHANGING, changingTo);
    }

    $changeTo(changeTo) {
        return new Promise((resolve) => {
            this.once(Events.SIDEBAR.OPEN_CHANGED, () => {
                this.setState({open: changeTo});
                resolve();
            });
            this.$emitOpenChanging(changeTo);
        })
    }

    close() {
        return this.$changeTo(false);
    }

    open() {
        return this.$changeTo(true);
    }

    toggle() {
        return this.$changeTo(!this.state.open);
    }
}

export default new Sidebar();