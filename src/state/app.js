import Base from './base';
import events from "../core/events";
import search from './../core/search';
import $matchAllFilter from './../core/filter/matchAll';
import enUSLocale from 'ro-webapp-backend/i18n/en-US';
import debug from 'debug';

const log = debug('ro-webapp:appState');

class App extends Base {
    constructor() {
        super();
        this.state = {
            online: null, //bool
            installed: null, //bool
            browser: null, //object
            webp: null, //bool
            settings: {}, //object
            screens: {}, //object
            locationAvailable: null,
            locationBlocked: null,
            location: null,
            activeScreen: null, //object
            languages: {
                'en-US': enUSLocale
            }, //object
            locale: null, //string
            searchFilter: $matchAllFilter, //function
            searchIn: search.SEARCH_IN.PLANTS //string
        }
    }

    setSearchIn(searchIn) {
        if (Object.values(search.SEARCH_IN).indexOf(searchIn) === -1) {
            console.warn('Unknown search in ', searchIn);
            return;
        }
        this.setState({searchIn: searchIn});
        this.emit(events.SEARCH.CHANGED, {searchFilter: this.state.searchFilter, searchIn});
    }

    setSearchFilter(searchFilter) {
        this.setState({searchFilter: searchFilter});
        this.emit(events.SEARCH.CHANGED, {searchFilter, searchIn: this.state.searchIn});
    }

    registerScreen(screenInstance) {
        let newScreens = {};
        newScreens[screenInstance.code] = screenInstance;
        this.setState({screens: Object.assign({}, this.state.screens, newScreens)});
    }

    setActiveScreen(screenInstance) {
        log('Active screen set to ', screenInstance.code)
        if (screenInstance.appTitle) {
            document.title = screenInstance.appTitle;
        }
        this.setState({activeScreen: screenInstance});
        //TODO: chnage on screens not propagated anywhere!
        Object.keys(this.state.screens).map((screenCode) => {
            this.state.screens[screenCode].active = false;
        })
        screenInstance.active = true;
    }

    addLanguage(locale, language) {
        if (this.state.languages[locale]) {
            return;
        }
        let newLocale = {};
        newLocale[locale] = language;
        this.setState({languages: Object.assign({}, this.state.languages, newLocale)});
        this.emit(events.APPLICATION.LANGUAGE_ADDED, locale)
    }

    setOnline(online) {
        this.setState({online: online});
    }

    getScreens() {
        return this.state.screens;
    }

    setWebPSupport(supported) {
        this.setState({webp: supported});
    }

    setInstalled(installed) {
        this.setState({installed: installed});
    }

    setBrowser(browser) {
        this.setState({browser: browser});
    }

    setSettings(settings) {
        this.setState({settings: settings});
    }

    setLocale(locale) {
        if (this.locale === locale) {
            return;
        }
        this.locale = locale;
        this.emit(events.APPLICATION.LANGUAGE_CHANGED, locale);
    }
}

export default new App();