import Database from './../core/database';
import Request from '../core/jsonRequest';
import $matchAllFilter from './../core/filter/matchAll';
import $defaultPlantsSort from './../core/filter/defaultPlantsSort';
import appState from './../state/app';
import AppSettings from 'ro-webapp-backend/lib/models/app/settingsNames';

const DB_NAME = 'ro_webapp_markers';

class markersDb extends Database {
    constructor(options) {
        super(DB_NAME, options);
        this.remoteMarkersTemp = null;
    }

    // async $fetchLocalRowsIndex() {
    //     let allDocs = await this.$fetchAllLocalRows();
    //     let index = {};
    //     if (allDocs.length === 0) {
    //         return {};
    //     }
    //
    //     allDocs.map((item) => {
    //         index[item.type + '_' + item.id] = item.updated_at;
    //     });
    //     return index;
    // }

    async $fetchRemoteRowsIndex() {
        return new Request('/api/v1.0/markers', {method: 'get'}).fetch()
        .then((rows) => {
            this.remoteMarkersTemp = rows;
            let index = {};
            rows.map((row) => {
                index[row.type + '_' + row.id] = row.updated_at;
            });
            console.log('remote idx', index);
            return index;
        })
    }

    async sync() {
        let updateInfo = await this.getUpdateInfo();
        if (!updateInfo.needsUpdate) {
            let docCount = await this.$getDocCount();
            return {docCount: docCount};
        }

        await this.clearAll();
        let count = await this.$getDocCount();
        for (let i = 0; i < this.remoteMarkersTemp.length; i++) {
            this.remoteMarkersTemp[i].id = this.remoteMarkersTemp[i].type + '_' + this.remoteMarkersTemp[i].id;
            await this.$addRow(this.remoteMarkersTemp[i]);
        }
        let docCount = await this.$getDocCount();
        return {docCount: docCount};
    }

    //not needed!
    async search(filter = $matchAllFilter, sort = $defaultPlantsSort) {
        return super.search(filter, sort);
    }
}

export default {NAME: DB_NAME, Database: markersDb};