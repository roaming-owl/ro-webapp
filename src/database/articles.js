import Database from './../core/database';
import Request from '../core/jsonRequest';
import $matchAllFilter from './../core/filter/matchAll';
import $defaultPlantsSort from './../core/filter/defaultPlantsSort';
import appState from './../state/app';
import AppSettings from 'ro-webapp-backend/lib/models/app/settingsNames';

import debug from 'debug';

const log = debug('ro-webapp:articles-db');


const DB_NAME = 'ro_webapp_articles';

class articlesDb extends Database {
    constructor(options) {
        super(DB_NAME, options);
    }

    async $fetchRemoteRowsIndex() {
        return new Request(appState.state.settings[AppSettings.BASE_URL] + '/api/v1.0/articles/index', {method: 'get'}).fetch();
    }

    async $fetchRemoteRow(id) {
        return new Request(appState.state.settings[AppSettings.BASE_URL] + '/api/v1.0/articles/' + id, {method: 'get'}).fetch();
    }

    async sync() {
        log('Syncing ', this.name);
        let updateInfo = await this.getUpdateInfo();
        log('Update nfo ', updateInfo);
        if (!updateInfo.needsUpdate) {
            let docCount = await this.$getDocCount();
            return {docCount: docCount};
        }

        await this.clearAll();
        let page = 1;
        let rowsPerPage = 50;
        let fetchedCount = 0;
        let count = null;
        while (fetchedCount < count || count === null) {
            let request = new Request('/api/v1.0/articles?page=' + page + '&rowsPerPage=' + rowsPerPage, {
                method: 'get'
            });
            let response = await request.fetch();
            log(`Got ${response.rows.length} articles`);
            for (let i = 0; i < response.rows.length; i++) {
                await this.$addRow(response.rows[i]);
            }
            page++;
            fetchedCount += response.rows.length;
            count = response.count;
        }
        let docCount = await this.$getDocCount();
        return {docCount: docCount};
    }
}

export default {NAME: DB_NAME, Database: articlesDb};