import Database from './../core/database';
import Request from '../core/jsonRequest';
import $matchAllFilter from './../core/filter/matchAll';
import $defaultPlantsSort from './../core/filter/defaultPlantsSort';
import appState from './../state/app';
import AppSettings from 'ro-webapp-backend/lib/models/app/settingsNames';

const DB_NAME = 'ro_webapp_plants_search_index';

class PlantsSearchIndexDb extends Database {
    constructor(options) {
        super(DB_NAME, options);
    }

    async $fetchRemoteRowsIndex() {
        return new Request('/api/v1.0/search-index/index?searchIn=plants', {method: 'get'}).fetch();
    }

    async $fetchRemoteRow(id) {
        return new Request('/api/v1.0/search-index/' + id + '/?searchIn=plants', {method: 'get'}).fetch();
    }

    async search(filter = $matchAllFilter, sort = $defaultPlantsSort) {
        return super.search(filter, sort);
    }
}

export default {NAME: DB_NAME, Database: PlantsSearchIndexDb};