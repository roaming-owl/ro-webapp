import Database from './../core/database';
import Request from '../core/jsonRequest';
import $matchAllFilter from './../core/filter/matchAll';
import $defaultPlantsSort from './../core/filter/defaultSort';
import appState from './../state/app';
import AppSettings from 'ro-webapp-backend/lib/models/app/settingsNames';

const DB_NAME = 'ro_webapp_expositions_search_index';

class ExpositionsSearchIndexDb extends Database {
    constructor(options) {
        super(DB_NAME, options);
    }

    async $fetchRemoteRowsIndex() { //TODO: normalize after settings loaded!
        return new Request('/api/v1.0/search-index/index?searchIn=expositions', {method: 'get'}).fetch();
    }

    async $fetchRemoteRow(id) {
        return new Request('/api/v1.0/search-index/' + id + '?searchIn=expositions', {method: 'get'}).fetch();
    }

    //TODO: use someting specific for expositions!
    // async search(filter = $matchAllFilter, sort = $defaultPlantsSort) {
    //     return super.search(filter, sort);
    // }
}

export default {NAME: DB_NAME, Database: ExpositionsSearchIndexDb};